package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Alpha <-- alpha
func Alpha(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Alpha, nd.Types)

	var m *pegn.Mark

	// alpha
	m = p.Check(is.Alpha)
	if m == nil {
		return expected("alpha", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
