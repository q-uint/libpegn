package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// ClassExpr <-- Simple (Spacing '/' SP+ Simple)*
func ClassExpr(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ClassExpr, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// Simple
	n, err = Simple(p)
	if err != nil {
		p.Goto(beg)
		return expected("Simple", node, p)
	}
	node.AdoptFrom(n)

	// (Spacing '/' SP+ Simple)*
	for {

		var err error
		b := p.Mark()
		var m *pegn.Mark

		// Spacing
		sp, err := Spacing(p)
		if err != nil {
			p.Goto(b)
			break
		}

		// '/' SP+
		m = p.Expect('/', is.Min{' ', 1})
		if m == nil {
			p.Goto(b)
			break
		}

		// Simple
		n, err = Simple(p)
		if err != nil {
			p.Goto(b)
			break
		}

		node.AdoptFrom(sp)
		node.AdoptFrom(n)

	}

	return node, nil
}
