package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// PatchVer <-- digit+
func PatchVer(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.PatchVer, nd.Types)

	var m *pegn.Mark

	// digit+
	m = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return expected("digit+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
