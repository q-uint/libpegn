package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Grammar <-- Meta? ComEndLine* (Definition ComEndLine*)+
func Grammar(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Grammar, nd.Types)

	var err error
	var count int
	var n *pegn.Node

	// Meta
	n, err = Meta(p)
	if err == nil {
		node.AppendChild(n)
	}

	// ComEndLine*
	for {
		n, err = ComEndLine(p)
		if err != nil {
			break
		}
		node.AdoptFrom(n)
	}

	// (Definition ComEndLine*)+
	count = 0
	for {

		// Definition
		n, err = Definition(p)
		if err != nil {
			break
		}
		node.AdoptFrom(n)

		// ComEndLine*
		for {
			n, err = ComEndLine(p)
			if err != nil {
				break
			}
			node.AdoptFrom(n)
		}

		count++
	}

	if count == 0 {
		return expected("(Definition ComEndLine*)+", node, p)
	}

	return node, nil
}
