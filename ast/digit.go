package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Digit <-- digit
func Digit(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Digit, nd.Types)

	var m *pegn.Mark

	// digit
	m = p.Check(is.Digit)
	if m == nil {
		return expected("digit", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
