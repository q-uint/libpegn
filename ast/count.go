package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Count <-- '{' digit+ '}'
func Count(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Count, nd.Types)

	var m *pegn.Mark

	// '{'
	if p.Expect('{') == nil {
		return expected("'{'", node, p)
	}

	// digit+
	m = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return expected("digit+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	// '}'
	if p.Expect('}') == nil {
		return expected("'}'", node, p)
	}

	return node, nil
}
