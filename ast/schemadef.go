package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// SchemaDef <-- CheckId SP+ '<--' SP+ Expression
func SchemaDef(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.SchemaDef, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// CheckId
	n, err = CheckId(p)
	if err != nil {
		p.Goto(beg)
		return expected("CheckId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<--' SP+
	if p.Expect(is.Min{' ', 1}, "<--", is.Min{' ', 1}) == nil {
		p.Goto(beg)
		return expected("SP+ '<--' SP+", node, p)
	}

	// Expression
	n, err = Expression(p)
	if err != nil {
		p.Goto(beg)
		return expected("Expression", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
