package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// MinMax <-- '{' Min ',' Max? '}'
func MinMax(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.MinMax, nd.Types)

	var n *pegn.Node
	var err error
	beg := p.Mark()

	// '{'
	if p.Expect('{') == nil {
		return expected("'{'", node, p)
	}

	// Min
	n, err = Min(p)
	if err != nil {
		p.Goto(beg)
		return expected("Min", node, p)
	}
	node.AppendChild(n)

	// ','
	if p.Expect(',') == nil {
		p.Goto(beg)
		return expected("','", node, p)
	}

	// Max
	n, err = Max(p)
	if err == nil {
		node.AppendChild(n)
	}

	// '}'
	if p.Expect('}') == nil {
		p.Goto(beg)
		return expected("'}'", node, p)
	}

	return node, nil
}
