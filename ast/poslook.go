package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// PosLook <-- '&' Primary Quant?
func PosLook(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.PosLook, nd.Types)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '&'
	if p.Expect('&') == nil {
		p.Goto(b)
		return expected("'%'", node, p)
	}

	// Primary
	n, err = Primary(p)
	if err != nil {
		p.Goto(b)
		return expected("Primary", node, p)
	}
	node.AdoptFrom(n)

	// Quant?
	n, err = Quant(p)
	if err == nil {
		node.AdoptFrom(n)
	}

	return node, nil
}
