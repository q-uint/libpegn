package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// BinRange <-- '[' Bin '-' Bin ']'
func BinRange(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.BinRange, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	if p.Expect('[') == nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Binary
	n, err = Binary(p)
	if err != nil {
		p.Goto(beg)
		return expected("Binary", node, p)
	}
	node.AppendChild(n)

	// '-'
	if p.Expect('-') == nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Binary
	n, err = Binary(p)
	if err != nil {
		p.Goto(beg)
		return expected("Binary", node, p)
	}
	node.AppendChild(n)

	// ']'
	if p.Expect(']') == nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
