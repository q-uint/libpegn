package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Unicode <-- 'U+' hexdig{4,8}
func Unicode(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Unicode, nd.Types)

	var m *pegn.Mark

	// 'U+' upperhex{4,8}
	m = p.Check("U+", is.MinMax{is.UpperHex, 4, 8})
	if m == nil {
		return expected("'U+' upperhex{4,8}", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
