package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// CheckDef <-- CheckId SP+ '<-' SP+ Expression
func CheckDef(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.CheckDef, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// CheckId
	n, err = CheckId(p)
	if err != nil {
		p.Goto(beg)
		return expected("CheckId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		p.Goto(beg)
		return expected("SP+ '<-' SP+", node, p)
	}

	// Expression
	n, err = Expression(p)
	if n == nil {
		p.Goto(beg)
		return expected("Expression", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
