package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// EndLine <-- LF / CR LF / CR
func EndLine(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.EndLine, nd.Types)

	var m *pegn.Mark

	// LF / CR LF / CR
	for {

		// LF
		m = p.Check('\n')
		if m != nil {
			break
		}

		// CR LF
		m = p.Check("\r\n")
		if m != nil {
			break
		}

		// CR
		m = p.Check('\r')
		if m != nil {
			break
		}

		return expected("LF / CR LF / CR", node, p)
	}

	node.Value += p.Parse(m)
	p.Goto(m)
	p.NewLine()
	p.Next()

	return node, nil
}
