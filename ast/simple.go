package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Simple <- Rune / ClassId / TokenId / Range / SQ String SQ
func Simple(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Simple, nd.Types)

	var err error
	var n *pegn.Node

	// Rune
	n, err = Rune(p)
	if err == nil {
		node.AdoptFrom(n)
		return node, nil
	}

	// ClassId
	n, err = ClassId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// TokenId
	n, err = TokenId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// Range
	n, err = Range(p)
	if err == nil {
		node.AdoptFrom(n)
		return node, nil
	}

	b := p.Mark()

	// SQ
	if p.Expect('\'') == nil {
		p.Goto(b)
		return expected("SQ", node, p)
	}

	// String
	n, err = String(p)
	if err != nil {
		p.Goto(b)
		return expected("String", node, p)
	}

	// SQ
	if p.Expect('\'') == nil {
		p.Goto(b)
		return expected("SQ", node, p)
	}

	node.AppendChild(n)
	return node, nil
}
