package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Spacing <- ComEndLine? SP+
func Spacing(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Spacing, nd.Types)
	var n *pegn.Node
	var err error

	// ComEndLine?
	n, err = ComEndLine(p)
	if err == nil {
		node.AdoptFrom(n)
	}

	// SP+
	if p.Expect(is.Min{' ', 1}) == nil {
		return expected("SP+", node, p)
	}

	return node, nil
}
