package ast

import (
	"fmt"

	pegn "gitlab.com/pegn/libpegn"
)

const (
	LANG          = `PEGN`
	LANG_EXT      = ``
	VERSION_MAJOR = 1
	VERSION_MINOR = 0
	VERSION_PATCH = 0
)

func expected(a string, node *pegn.Node, p pegn.Parser) (*pegn.Node, error) {
	return nil, fmt.Errorf("expected %v while parsing %v at %v",
		a, node.TypeName(), p.Mark())
}
