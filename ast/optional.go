package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Optional <-- '?'
func Optional(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Optional, nd.Types)
	node.Value = "?"

	if p.Expect('?') == nil {
		return expected("'?'", node, p)
	}

	return node, nil
}
