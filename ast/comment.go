package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Comment <-- (!EndLine any)+
func Comment(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Comment, nd.Types)

	var err error
	var m *pegn.Mark
	var count int

	// (!EndLine any)*
	count = 0
	for {

		b := p.Mark()

		// !EndLine
		_, err = EndLine(p)
		if err == nil {
			p.Goto(b)
			break
		}

		// any
		m = p.Check(is.Any)
		if m == nil {
			p.Goto(b)
			break
		}
		node.Value += p.Parse(m)
		p.Next()

		count++
	}

	if !(count >= 1) {
		return expected("(!EndLine any)*", node, p)
	}

	return node, nil
}
