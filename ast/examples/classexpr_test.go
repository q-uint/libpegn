package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleClassExpr_ranges() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[A-Z] / [a-z]")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["AlphaRange", [
	//     ["Alpha", "A"],
	//     ["Alpha", "Z"]
	//   ]],
	//   ["AlphaRange", [
	//     ["Alpha", "a"],
	//     ["Alpha", "z"]
	//   ]]
	// ]]
}

func ExampleClassExpr_classes() {
	var n *pegn.Node
	p := parser.New()
	p.Init("alphanum / punct")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["ClassId", [
	//     ["ResClassId", "alphanum"]
	//   ]],
	//   ["ClassId", [
	//     ["ResClassId", "punct"]
	//   ]]
	// ]]
}

func ExampleClassExpr_tokens() {
	var n *pegn.Node
	p := parser.New()
	p.Init("SP / TAB / CR / LF")
	n, _ = ast.ClassExpr(p)
	n.Print()
	// Output:
	// ["ClassExpr", [
	//   ["TokenId", [
	//     ["ResTokenId", "SP"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "TAB"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "CR"]
	//   ]],
	//   ["TokenId", [
	//     ["ResTokenId", "LF"]
	//   ]]
	// ]]
}
