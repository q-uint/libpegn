package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleComEndLine_empty() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := parser.New()

	// SP\n
	p.Init(" \n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["EndLine", "\n"]
	// ]]

}

func ExampleComEndLine_plaintxt() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := parser.New()

	p.Init("   # Comment here.\n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["Comment", "Comment here."],
	//   ["EndLine", "\n"]
	// ]]

}

func ExampleComEndLine_unicode() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := parser.New()

	p.Init("   # Comment👌 here.\n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["Comment", "Comment👌 here."],
	//   ["EndLine", "\n"]
	// ]]

}
