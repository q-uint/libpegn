package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleComment_none() {
	var n *pegn.Node
	p := parser.New()
	p.Init("\n")
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// <nil>
}

func ExampleComment_space() {
	var n *pegn.Node
	p := parser.New()
	// SP
	p.Init(" ")
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// ["Comment", " "]
}

func ExampleComment_simple() {
	var n *pegn.Node
	p := parser.New()
	// Comment 👌 here.
	str := "Comment 👌 here."
	p.Init(str)
	n, _ = ast.Comment(p)
	n.Print()
	// Output:
	// ["Comment", "Comment 👌 here."]
}
