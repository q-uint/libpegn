package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleOctRange() {
	var n *pegn.Node

	// OctRange <-- "[" Octal "-" Octal "]"
	p := parser.New()

	// [o0-o7]
	p.Init("[o0-o7]")
	n, _ = ast.OctRange(p)
	n.Print()

	// [o00-o77]
	p.Init("[o00-o77]")
	n, _ = ast.OctRange(p)
	n.Print()

	// Output:
	// ["OctRange", [
	//   ["Octal", "o0"],
	//   ["Octal", "o7"]
	// ]]
	// ["OctRange", [
	//   ["Octal", "o00"],
	//   ["Octal", "o77"]
	// ]]

}
