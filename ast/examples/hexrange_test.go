package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleHexRange() {
	var n *pegn.Node

	// HexRange <-- "[" Hexadec "-" Hexadec "]"
	p := parser.New()

	// [x0-xF]
	p.Init("[x0-xF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// [x00-xFF]
	p.Init("[x00-xFF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// Output:
	// ["HexRange", [
	//   ["Hexadec", "x0"],
	//   ["Hexadec", "xF"]
	// ]]
	// ["HexRange", [
	//   ["Hexadec", "x00"],
	//   ["Hexadec", "xFF"]
	// ]]

}
