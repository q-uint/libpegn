package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleHexadec() {
	var n *pegn.Node

	// Hexadec <-- "x" upperhex+
	p := parser.New()

	// x0A
	p.Init("x0A")
	n, _ = ast.Hexadec(p)
	n.Print()

	// xAAA
	p.Init("xAAA")
	n, _ = ast.Hexadec(p)
	n.Print()

	// Output:
	// ["Hexadec", "x0A"]
	// ["Hexadec", "xAAA"]

}
