package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleUnicode() {

	var n *pegn.Node

	// Unicode <-- "U+" upperhex{4,8}
	p := parser.New()

	// U+0000
	p.Init("U+0000")
	n, _ = ast.Unicode(p)
	n.Print()

	// U+ffffff
	p.Init("U+ffffff")
	n, _ = ast.Unicode(p)
	n.Print()

	// U+FFFFFF
	p.Init("U+FFFFFF")
	n, _ = ast.Unicode(p)
	n.Print()

	// Output:
	// ["Unicode", "U+0000"]
	// <nil>
	// ["Unicode", "U+FFFFFF"]

}
