package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleBinary() {

	var n *pegn.Node

	// Binary <-- "b" bitdig+
	p := parser.New()

	// b01
	p.Init("b01")
	n, _ = ast.Binary(p)
	n.Print()

	// b01010110
	p.Init("b01010110")
	n, _ = ast.Binary(p)
	n.Print()

	// Output:
	// ["Binary", "b01"]
	// ["Binary", "b01010110"]

}
