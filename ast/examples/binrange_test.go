package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleBinRange() {

	var n *pegn.Node

	// BinRange <-- "[" Bin "-" Bin "]"
	p := parser.New()

	// [b01-b10]
	p.Init("[b01-b10]")
	n, _ = ast.BinRange(p)
	n.Print()

	// [b0001-b1111]
	p.Init("[b0001-b1111]")
	n, _ = ast.BinRange(p)
	n.Print()

	// Output:
	// ["BinRange", [
	//   ["Binary", "b01"],
	//   ["Binary", "b10"]
	// ]]
	// ["BinRange", [
	//   ["Binary", "b0001"],
	//   ["Binary", "b1111"]
	// ]]

}
