package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleOptional() {
	var n *pegn.Node

	// Optional <-- "?"
	p := parser.New()

	// ?
	p.Init("?")
	n, _ = ast.Optional(p)
	n.Print()

	// Output:
	// ["Optional", "?"]

}
