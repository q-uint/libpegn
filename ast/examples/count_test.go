package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleCount() {
	var n *pegn.Node

	// Count <-- "{" digit+ "}"
	p := parser.New()

	// {1}
	p.Init("{1}")
	n, _ = ast.Count(p)
	n.Print()

	// {99}
	p.Init("{99}")
	n, _ = ast.Count(p)
	n.Print()

	// Output:
	// ["Count", "1"]
	// ["Count", "99"]

}
