package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMajorVer() {
	var n *pegn.Node

	// MajorVer <-- digit+
	p := parser.New()

	// 0
	p.Init("0")
	n, _ = ast.MajorVer(p)
	n.Print()

	// Output:
	// ["MajorVer", "0"]

}
