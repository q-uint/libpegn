package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleUniRange() {

	var n *pegn.Node

	// UniRange <-- "[" Unicode "-" Unicode "]"
	p := parser.New()

	// [U+0000-U+00FF]
	p.Init("[U+0000-U+00FF]")
	n, _ = ast.UniRange(p)
	n.Print()

	// [U+0000FF-U+00FFFF]
	p.Init("[U+0000FF-U+00FFFF]")
	n, _ = ast.UniRange(p)
	n.Print()

	// Output:
	// ["UniRange", [
	//   ["Unicode", "U+0000"],
	//   ["Unicode", "U+00FF"]
	// ]]
	// ["UniRange", [
	//   ["Unicode", "U+0000FF"],
	//   ["Unicode", "U+00FFFF"]
	// ]]

}
