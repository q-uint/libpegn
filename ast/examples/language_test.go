package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleLanguage() {
	var n *pegn.Node

	// Language <- Lang ("-" LangExt)?
	p := parser.New()

	// PEGN
	p.Init("PEGN")
	n, _ = ast.Language(p)
	n.Print()

	// PEGN-alpa
	p.Init("PEGN-alpha")
	n, _ = ast.Language(p)
	n.Print()

	// Output:
	// ["Language", [
	//   ["Lang", "PEGN"]
	// ]]
	// ["Language", [
	//   ["Lang", "PEGN"],
	//   ["LangExt", "alpha"]
	// ]]

}
