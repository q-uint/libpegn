package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleResTokenId() {

	var n *pegn.Node

	// ResTokenId <-- 'TAB' / 'LF' / 'CR' / 'CRLF' / 'SP' / 'VT' / 'FF' / 'NOT'
	//              / 'BANG' / 'DQ' / 'HASH' / 'DOLLAR' / 'PERCENT' / 'AND'
	//              / 'SQ' / 'LPAREN' / 'RPAREN' / 'STAR' / 'PLUS' / 'COMMA'
	//              / 'DASH' / 'MINUS' /'DOT' /'SLASH' /'COLON' /'SEMI' /'LT'
	//              / 'EQ' / 'GT' / 'QUERY' / 'QUESTION' / 'AT' / 'LBRAKT'
	//              / 'BKSLASH' / 'RBRAKT' / 'CARET' / 'UNDER' / 'BKTICK'
	//              / 'LCURLY' / 'LBRACE' / 'BAR' / 'PIPE' / 'RCURLY'
	//              / 'RBRACE' / 'TILDE' / 'UNKNOWN' / 'REPLACE' / 'MAXRUNE'
	//              / 'MAXASCII' / 'MAXLATIN' / 'LARROW' / 'RARROW' / 'LLARROW'
	//              / 'RLARROW' / 'LARROWF' / 'LFAT' / 'RARROWF' / 'RFAT'
	//              / 'WALRUS'
	p := parser.New()

	p.Init("LF")
	n, _ = ast.ResTokenId(p)
	n.Print()

	p.Init("CR")
	n, _ = ast.ResTokenId(p)
	n.Print()

	p.Init("LARROW")
	n, _ = ast.ResTokenId(p)
	n.Print()

	// Output:
	// ["ResTokenId", "LF"]
	// ["ResTokenId", "CR"]
	// ["ResTokenId", "LARROW"]

}
