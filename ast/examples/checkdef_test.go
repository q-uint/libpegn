package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleCheckDef_single() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Rule <- Another")
	n, _ = ast.CheckDef(p)
	n.Print()
	// Output:
	// ["CheckDef", [
	//   ["CheckId", "Rule"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Another"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExampleCheckDef_multi() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Simple <- ResClassId / ClassId / Etc")
	n, _ = ast.CheckDef(p)
	n.Print()
	// Output:
	// ["CheckDef", [
	//   ["CheckId", "Simple"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "ResClassId"]
	//       ]]
	//     ]],
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "ClassId"]
	//       ]]
	//     ]],
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Etc"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
