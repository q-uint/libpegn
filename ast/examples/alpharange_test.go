package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleAlphaRange() {

	var n *pegn.Node

	// AlphaRange <-- "[" Alpha "-" Alpha "]"
	p := parser.New()

	// [a-z]
	p.Init("[a-z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// [A-Z]
	p.Init("[A-Z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// Output:
	// ["AlphaRange", [
	//   ["Alpha", "a"],
	//   ["Alpha", "z"]
	// ]]
	// ["AlphaRange", [
	//   ["Alpha", "A"],
	//   ["Alpha", "Z"]
	// ]]

}
