package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMinMax() {
	var n *pegn.Node

	// MinMax <-- "{" Min "," Max? "}"
	p := parser.New()

	// {1,}
	p.Init("{1,}")
	n, _ = ast.MinMax(p)
	n.Print()

	// {0,9}
	p.Init("{0,9}")
	n, _ = ast.MinMax(p)
	n.Print()

	// Output:
	// ["MinMax", [
	//   ["Min", "1"]
	// ]]
	// ["MinMax", [
	//   ["Min", "0"],
	//   ["Max", "9"]
	// ]]

}
