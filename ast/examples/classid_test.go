package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleClassId() {
	var n *pegn.Node

	// ClassId <-- ResClassId / lower (lower / UNDER lower)+
	p := parser.New()

	p.Init("alphanum")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("any")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("classid")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("another_id")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("_another_id")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("an_valid_but_unexpected_")
	n, _ = ast.ClassId(p)
	n.Print()

	// Output:
	// ["ClassId", [
	//   ["ResClassId", "alphanum"]
	// ]]
	// ["ClassId", [
	//   ["ResClassId", "any"]
	// ]]
	// ["ClassId", "classid"]
	// ["ClassId", "another_id"]
	// <nil>
	// ["ClassId", "an_valid_but_unexpected"]

}
