package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// LangExt <-- visible{1,20}
func LangExt(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.LangExt, nd.Types)

	var m *pegn.Mark

	// visible{1,20}
	m = p.Check(is.MinMax{is.Visible, 1, 20})
	if m == nil {
		return expected("visible{1,20}", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
