// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the Apache
// 2.0 license that can be found in the LICENSE file.

/*
Package pegn (pronounced "pagan") contains a reference implementation
for the Parsing Expression Grammar Notation (PEGN) specification which
facilitates the creation and handling of language grammars including
domain-specific, markup, and others. This top-level package consists
mostly of interfaces and their expected behavior allowing anyone to
implement their own components without any specific implementation
dependency. Concrete default implementations exist in the parser and
node subpackages.
*/
package pegn

import "fmt"

// Parser is designed to be used by PEG-centric parser implementations
// that assume full input buffer loads with infinite look-ahead. This
// assumption allows looking far ahead (Look) for a series of matches
// and rewind (Goto) if any match in the series fails. Such parsers are
// designed primarily to be passed to parse functions such as those in
// the pegn.ast package providing a sustainable, modular architecture
// for creating and maintaining PEG and other modern approaches that
// combine the traditional lexing and parsing into one.
//
// TODO (finish writing up the method descriptions, especially Look)
//
// Expect(m ...interface{}) *pegn.Mark
//
// The Expect method looks ahead (starting with the current Rune) for
// a series of matches and returns the Mark of the last position if all
// matches are successful. Otherwise it returns nil. All Parser
// implementations must minimally support the following match types:
//
//   rune     - a single rune
//   string   - an exact string match
//   IsFunc   - single rune from a class represented by IsFunc
//   CheckFunc - individual look ahead using parser position
//
// Look(m ...interface{}) *pegn.Mark
//
// Look is identical to Expect except that it does not change the
// position of the parser when called. This can be
// accomplished by reverting to a saved position after calling
// Expect.
//
// Mark() *Mark
//
// Mark returns a pointer to a copy of the internal marker. This is
// necessary when complex parsing functions need to reposition back to
// the starting position after invoking other parse functions that would
// move the internal parser location. When passed to Goto() the parser
// reverts its state fully to that point when it was first pointing to
// the position marked.
//
// NewLine()
//
// Because every grammar may deal with new lines differently (some
// considering them tokens, others ignoring) the NewLine method is
// provided for parse functions to call.
//
// Errorf(str string, a ...interface{})
//
// Send the error to the internal error handler.
//
// Expecting(a ...interface{})
//
// Constructs an error using the passed arguments and passes it to the
// internal error handler. If one argument assumes the Node integer
// type. If two assumes Node integer type and the containing Node as
// well. If no arguments or more than 2 arguments simply sends a generic
// "unexpected" error.
type Parser interface {
	Init(s interface{}) error          // init, buffer all input, mark first
	Mark() *Mark                       // returns copy of internal input mark
	Peek() rune                        // peek at the next rune
	Check(m ...interface{}) *Mark      // check for matches without advancing
	Expect(m ...interface{}) *Mark     // expect matches and advance
	Goto(m *Mark)                      // set parser to given mark position
	Next()                             // move internal mark one rune forward
	Move(n int)                        // move n runes forward
	Done() bool                        // end of data
	Parse(m *Mark) string              // returns data to mark
	Slice(beg *Mark, end *Mark) string // returns data between inclusively
	NewLine()                          // forces newline in internal mark
	fmt.Stringer                       // delegate to Mark.String()
	Print()                            // prints self
}
