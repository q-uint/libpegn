package pegn_test

import (
	"fmt"

	pegn "gitlab.com/pegn/libpegn"
)

func ExampleJSONString() {

	o := fmt.Println
	o(pegn.JSONString("something"))
	o(pegn.JSONString("some\tTAB"))
	o(pegn.JSONString("some\bbell"))
	o(pegn.JSONString("some\fFF"))
	o(pegn.JSONString("some\nLF"))
	o(pegn.JSONString("some\rCR"))
	o(pegn.JSONString("some\\BKSLASH"))
	o(pegn.JSONString("some\"DQ"))
	o(pegn.JSONString("some 👌 thing"))
	o(pegn.JSONString("just / slash"))

	// Output:
	// something
	// some\tTAB
	// some\bbell
	// some\fFF
	// some\nLF
	// some\rCR
	// some\\BKSLASH
	// some\"DQ
	// some 👌 thing
	// just / slash

}
