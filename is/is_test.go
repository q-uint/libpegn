package is_test

import (
	"fmt"

	"gitlab.com/pegn/go/is"
)

var o = fmt.Println

func ExampleAny() {
	o(is.Any(-1))
	o(is.Any('\u0000'))
	o(is.Any(' '))
	o(is.Any('\U0010FFFF'))
	// Output:
	// false
	// true
	// true
	// true
}

func ExampleUpper() {
	o(is.Upper('a'))
	o(is.Upper('1'))
	o(is.Upper('A'))
	o(is.Upper('_'))
	// Output:
	// false
	// false
	// true
	// false
}

func ExampleLower() {
	o(is.Lower('A'))
	o(is.Lower('1'))
	o(is.Lower('a'))
	o(is.Lower('_'))
	// Output:
	// false
	// false
	// true
	// false
}

func ExampleAlpha() {
	o(is.Alpha('a'))
	o(is.Alpha('A'))
	o(is.Alpha('_'))
	// Output:
	// true
	// true
	// false
}

func ExampleDigit() {
	o(is.Digit('2'))
	o(is.Digit('-'))
	// Output:
	// true
	// false
}

func ExamplePunct() {
	o(is.Punct('!'))
	o(is.Punct('_'))
	o(is.Punct('-'))
	o(is.Punct('\''))
	o(is.Punct(100))
	o(is.Punct('a'))
	o(is.Punct('👿'))
	// Output:
	// true
	// true
	// true
	// true
	// false
	// false
	// false
}

func ExampleAlphaNum() {
	o(is.AlphaNum('a'))
	o(is.AlphaNum('A'))
	o(is.AlphaNum('0'))
	o(is.AlphaNum('9'))
	o(is.AlphaNum('_'))
	// Output:
	// true
	// true
	// true
	// true
	// false
}

func ExampleHex() {
	o(is.Hexdig('0'))
	o(is.Hexdig('9'))
	o(is.Hexdig('a'))
	o(is.Hexdig('A'))
	o(is.Hexdig('f'))
	o(is.Hexdig('F'))
	o(is.Hexdig('g'))
	o(is.Hexdig('G'))
	// Output:
	// true
	// true
	// true
	// true
	// true
	// true
	// false
	// false
}

func ExampleLowerHex() {
	o(is.LowerHex('0'))
	o(is.LowerHex('9'))
	o(is.LowerHex('a'))
	o(is.LowerHex('A'))
	o(is.LowerHex('f'))
	o(is.LowerHex('F'))
	o(is.LowerHex('g'))
	o(is.LowerHex('G'))
	// Output:
	// true
	// true
	// true
	// false
	// true
	// false
	// false
	// false
}

func ExampleUpperHex() {
	o(is.UpperHex('0'))
	o(is.UpperHex('9'))
	o(is.UpperHex('a'))
	o(is.UpperHex('A'))
	o(is.UpperHex('f'))
	o(is.UpperHex('F'))
	o(is.UpperHex('g'))
	o(is.UpperHex('G'))
	// Output:
	// true
	// true
	// false
	// true
	// false
	// true
	// false
	// false
}

func ExampleVisible() {
	o(is.Visible(' '))
	o(is.Visible('\t'))
	o(is.Visible('\n'))
	o(is.Visible('\r'))
	o(is.Visible('a'))
	o(is.Visible('0'))
	o(is.Visible('_'))
	o(is.Visible('`'))
	// Output:
	// false
	// false
	// false
	// false
	// true
	// true
	// true
	// true
}

func ExampleWhiteSpace() {
	o(is.WhiteSpace('a'))
	o(is.WhiteSpace(' '))
	o(is.WhiteSpace('\t'))
	o(is.WhiteSpace('\r'))
	o(is.WhiteSpace('\n'))
	// Output:
	// false
	// true
	// true
	// true
	// true
}

func ExampleQuotable() {
	o(is.Quotable('.'))
	o(is.Quotable(' '))
	o(is.Quotable('\t'))
	o(is.Quotable('\r'))
	o(is.Quotable('🔥'))
	// Output:
	// true
	// true
	// false
	// false
	// false
}
